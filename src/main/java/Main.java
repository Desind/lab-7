import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    public static final String DEST = "CV.pdf";
    public static void main(String[] args){
        try{
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(DEST));
            document.open();
            addMetaData(document);
            createTable(document);
            document.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private static void createTable(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("CV Lukasz Zaranek", catFont));
        addEmptyLine(preface, 1);
        document.add(preface);
        addEmptyLine(preface, 3);

        PdfPTable table = new PdfPTable(2);
        table.addCell("First name: ");
        table.addCell("Lukasz");
        table.addCell("Last name:");
        table.addCell("Zaranek");
        table.addCell("Proffesion:");
        table.addCell("Java developer (to be)");
        table.addCell("Education:");
        table.addCell("2014-2018 ZSME w Tarnowie\n2018 - till today PWSZ w Tarnowie");
        document.add(table);

    }
    private static void addMetaData(Document document) {
        document.addTitle("CV Lukasz Zaranek");
        document.addSubject("CV");
        document.addKeywords("Java, PDF, iText, CV");
        document.addAuthor("Lukasz Zaranek");
        document.addCreator("Lukasz Zaranek");
    }
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
